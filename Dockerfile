FROM debian:testing-slim

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt update \
  && apt install -y ffmpeg jed less psmisc ssl-cert curl gnupg \
  nginx-light libnginx-mod-rtmp \
  && curl -sL https://deb.nodesource.com/setup_8.x | bash - \
  && apt install -y nodejs \
  && rm -rf /var/lib/apt/lists/*

COPY nginx_config/default /etc/nginx/sites-enabled/default
COPY nginx_config/nginx.conf /etc/nginx/nginx.conf

COPY html /var/www/html
RUN cd /var/www/html \
  && npm install video.js videojs-hls-source-selector videojs-flash \
  videojs-contrib-hls videojs-contrib-quality-levels \
  videojs-hls-source-selector

RUN mkdir /tmp/video_archiv && chown www-data /tmp/video_archiv

# EXPOSE 80 443 1935
EXPOSE 80 1935

STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]
