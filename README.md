# Live Video Server für die Perfomance "925" von Verena Rotky

[https://www.museum-joanneum.at/kioer/programm/events/event/7532/kioer-supports-1](https://www.museum-joanneum.at/kioer/programm/events/event/7532/kioer-supports-1)

Der hier vorliegende Live Video Streaming Server basiert auf
Vorarbeiten, die ursprünglich für das Forum Stadtpark entwickelt
wurden. Das Setup wurde nur ein wenig bereinigt, damit es nun auch
automatisch vom Git-Lab CI system in ein fertiges Docker-Image
übersetzt werden kann. Da das benötigte nginx-RTMP-modul für die
adaptive Videoauslieferung mittlerweile unter debian testing fertig
zur Verfügung steht, gestaltet sich das relativ einfach.

## Benutzung des Docker Images

Obwohl in der Regel nur ganz geringe Anpassungen für die praktische
Verwendung in unterschiedlichen Fällen vorzunehmen sind, orientiert
sich diese Anwendung weitestgehnd am Vorbild der "immutable
containers" -- d.h. Änderungen für andere Anwedungsfälle sollten an
einem geklonten Repository vorgenommen werden, das dann wieder
automatisch in einen fertigen Anwedungsconmtainer gepackt wird.

Eine Ausnahme von dieser Regel bildet das explizite forwarden der
benötigen TCP-Ports am Server, die aus Gründen der Übersichtlichkeit
bzw. besseren Nachvollziehbarkeit am Docker-Server nicht im Container
festgelegt werden. Deutlich vorbildlicher läßt sich das nur mit Hilfe
sauberer Container-Orchestrierung (bspw. mittels Kubernetes) verwalten.

Auch die beiden Verzeichnise mit den aufgezeichneten Video-Streams und
den temporären HLS Video-Fragmenten werden über explizit
Volume-Spezifikation eingebunden, um einerseits flexibler auf
Platzvorgaben am Server reagieren zu können, aber auch um effizientere
Schreibzugriffe zu gewährleisten als es die Docker-üblichen
Overlay-Filesysteme erlauben würden. 

Starten des Servers auf unserem experimentellen Docker Server:

    docker run -d --restart unless-stopped \
	-p 8925:80 -p 9925:1935 \
	--mount 'type=volume,src=live-video-verena,dst=/tmp/video_archive' \
	--mount 'type=tmpfs,dst=/tmp/hls' \
	--name live-video-verena \
	registry.gitlab.com/mur-at-public/live-video-verena 

Damit sollte die betreffende Webpage erreichbar sein:

[http://kaffee.mur.at:8925](http://kaffee.mur.at:8925)

Die aufgezeichneten Video-Files sind under folgendem URL zu finden:

[http://kaffee.mur.at:8925/video_archiv/](http://kaffee.mur.at:8925/video_archiv/)

## Updaten des Docker Images

Wenn im Git-Repository ändereung vorgenommen wurden und auf GitLab ein
neues Docker Image generiert wurde, muss dieses manuell installiert
werden, um es auch am lokalen Docker Server zu aktalisieren:

    docker pull registry.gitlab.com/mur-at-public/live-video-verena
	docker stop live-video-verena
	docker rm live-video-verena
	docker run ... [wie oben]

## Senden des Live Videos

Der Upstream der Live Übertragung erfolgt über das RTMP-Protokoll und
kann bspw. mittels `ffmpeg` aus einer V4L2 webcam quelle erfolgen:

    #!/bin/sh

    ffmpeg \
	-f alsa  -ac 1 -i hw:U0x46d0x81d \
	-f v4l2 -framerate 24 -input_format yuyv422 \
	-video_size 752x416 -i /dev/video0 \
	-hide_banner -af aresample=async=1000 -pix_fmt yuv420p -ac 2 \
	-c:v libx264 -preset fast -b:v 1400k \
	-c:a aac -b:a 128k -bufsize 256 \
	-f flv rtmp://kaffee.mur.at:9925/live/rotky-925

Am besten schreibt man diesen Befehl in einen shell-script (bspw. 
`/home/machine/rtmp-encoder.sh`) und erlaubt dessen Ausführung. 

(Im Falle der konreten Installation, die über eine LTE-Router
abgewicklet wurde, war zusätzlich noch ein Netzerk-Tunnel bzw. lokales
port-forwarding nötig, um eine funktionierende Verbindung zu erzielen:
`ssh -f -L 9925:kaffee.mur.at:9925 user@kaffee.mur.at -N`)

Um das script bei jedem Neustart via Systemd auszuführen, benötigt man
darüber hinaus auch noch ein Service-File folgender Form
`/lib/systemd/system/rtmp-encoder.service`:

```
[Unit]
Description=RTMP LiveStreaming Encoder
ConditionFileIsExecutable=/home/machine/rtmp-encoder.sh
After=network.eth0

[Service]
ExecStart=/home/machine/rtmp-encoder.sh
Restart=always
RestartSec=15

[Install]
WantedBy=multi-user.target
```

Nun muss der Service noch enabled und gestartet werden:

    sudo systemctl enable rtmp-encoder
    sudo systemctl start rtmp-encoder

Prinzipiell können auch mehrer Streams gleichzeit übertragen und
aufgezeichnet werden. Es muss dazu nur ein unterschiedlicher Name im
RTMP upstrem genutzt werden und die webseite mit dem Video-Viewer auf
das entsprechend modizierte .m3u8 HLS Manifest zeigen.

## Tipps zum Debuggen

Ob die `ffmpeg` worker tatsächlich aktiv sind, sieht man am besten mit
folgendem Befehl von außen:

```
ms@kaffee:~$ docker exec live-video-verena pstree
nginx-+-nginx---ffmpeg---19*[{ffmpeg}]
      `-2*[nginx]
```

Ähnliches gilt für das Überprüfen des HLS-Verzeichnises:

```
ms@kaffee:~$ docker exec live-video-verena ls /tmp/hls
rotky-925.m3u8
rotky-925_low-14.ts
rotky-925_low-15.ts
rotky-925_low-16.ts
rotky-925_low-17.ts
...
```

Und natürlich kann man den stream auch mit einem vernünftigen
Video-Player wiedergeben:

    mpv -opengl-backend x11 http://kaffee.mur.at:8925/hls/rotky-925.m3u8
	
